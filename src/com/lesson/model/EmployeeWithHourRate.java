package com.lesson.model;


public class EmployeeWithHourRate extends Employee {

    float salaryPerHour;
    float hoursPerMonth;
    Currency currency;

    public EmployeeWithHourRate(String name, String post, Currency currency, float salaryPerHour, float hoursPerMonth) {
        super(name, post);
        this.salaryPerHour = salaryPerHour;
        this.hoursPerMonth = hoursPerMonth;
        this.currency = currency;
    }
    @Override
    public float getMonthValue() {
        return this.salaryPerHour * this.hoursPerMonth * currency.sellCurrencyRate;

    }
    @Override
    public float getYearValue() {

        return  getMonthValue()* 12;
    }
}
