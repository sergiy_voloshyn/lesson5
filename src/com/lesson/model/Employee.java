package com.lesson.model;


public abstract class Employee {
    public String name;
    public String post;

    abstract public float getMonthValue();
    abstract public float getYearValue();



    public Employee(String name, String post) {
        this.name = name;
        this.post = post;

    }

}
