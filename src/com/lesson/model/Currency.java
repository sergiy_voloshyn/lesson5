package com.lesson.model;


public class Currency {
    String name;
    float sellCurrencyRate;

    public Currency(String name, float sellCurrency) {
        this.name = name;

        this.sellCurrencyRate = sellCurrency;
    }
}

