package com.lesson.model;


public class EmployeeWithMonthlySalary extends Employee{
    float salaryPerMonth;

    public EmployeeWithMonthlySalary(String name, String post, float salaryPerMonth) {
        super(name, post);
        this.salaryPerMonth = salaryPerMonth;
    }
    @Override
    public float getMonthValue() {
        return this.salaryPerMonth;

    }
    @Override
    public float getYearValue() {
        return this.salaryPerMonth*12;
    }
}


