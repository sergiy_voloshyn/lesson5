package com.lesson.model;

/**
 * Created by user on 03.12.2017.
 */
public class EmployeeWithPercentSales extends Employee {

    float percent;
    float amountOfSalesFirstHalfYear;
    float amountOfSalesSecondHalfYear;

    public EmployeeWithPercentSales(String name, String post, float percent, float amountOfSalesFirstHalfYear, float amountOfSalesSecondHalfYear) {
        super(name, post);
        this.percent = percent;
        this.amountOfSalesFirstHalfYear = amountOfSalesFirstHalfYear;
        this.amountOfSalesSecondHalfYear = amountOfSalesSecondHalfYear;
    }
    @Override
    public float getMonthValue() {
        return getYearValue()/12;

    }

    @Override
    public float getYearValue() {
        return amountOfSalesFirstHalfYear / 100 * percent * 6 + amountOfSalesSecondHalfYear / 100 * percent * 6;
    }
}
