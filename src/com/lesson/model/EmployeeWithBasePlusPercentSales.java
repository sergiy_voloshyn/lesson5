package com.lesson.model;

/**
 * Created by user on 03.12.2017.
 */
public class EmployeeWithBasePlusPercentSales extends Employee {
    float percent;
    float amountOfSalesFirstHalfYear;
    float amountOfSalesSecondHalfYear;
    float baseRate;


    public EmployeeWithBasePlusPercentSales(String name, String post, float percent, float amountOfSalesFirstHalfYear,
                                            float amountOfSalesSecondHalfYear, float baseRate) {
        super(name, post);
        this.percent = percent;
        this.amountOfSalesFirstHalfYear = amountOfSalesFirstHalfYear;
        this.amountOfSalesSecondHalfYear = amountOfSalesSecondHalfYear;
        this.baseRate = baseRate;
    }
    @Override
    public float getMonthValue() {
        return getYearValue()/12;
    }

    @Override
    public float getYearValue() {
        return (baseRate + amountOfSalesFirstHalfYear / 100 * percent) * 6 +
                (baseRate + amountOfSalesSecondHalfYear / 100 * percent) * 6;
    }
}
