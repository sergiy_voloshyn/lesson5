package com.lesson.util;

import com.lesson.model.*;

/**
 * Created by user on 03.12.2017.
 */
public final class Generator {

    private Generator() {
    }

    public static Employee[] generateEmployees() {

        Currency currency = new Currency("USD", 27);

        Employee[] employees = new Employee[4];
        employees[0] = new EmployeeWithMonthlySalary("Иванова Елена Львовна", "зам директора", 4500.0F);
        employees[1] = new EmployeeWithHourRate("Вакуленко Дмитрий Владимирович", "дизайнер",
                currency, 7.0F, 160.0F);
        employees[2] = new EmployeeWithPercentSales("Коренькова Анна Павловна", " менеджер по продажам",
                5.0F, 50000.0F,65000.0F);
        employees[3] = new EmployeeWithBasePlusPercentSales("Татьяна Сергеевна", " менеджер по продажам",
                5.0F, 50000.0F,65000.0F,1000.0F);

        return employees;
    }

}