package com.lesson.ui;

//import com.lesson.model.Employee;
import com.lesson.model.*;
import com.lesson.util.Generator;

/*
2. Написать программу "Бухгалтерия". Программа должна позволять рассчитать месячный и годовой доход каждого сотрудника,
 в зависимости от расчёта заработной платы. А именно:
- месячная ставка
- почасовая оплата
- % от объема продаж
- базовая месячная ставка + % от объема продаж
Для каждого сотрудника необходимо хранить ФИО и должность.
Проверить работу программы необходимо на следующих сотрудниках:
Иванова Елена Львовна, зам директора, оклад 4500грн
Вакуленко Дмитрий Владимирович, дизайнер, 7$ в час
Коренькова Анна Павловна, менеджер по продажам (получает 5% от продаж, первые полгода касса составляла 50000грн,
 вторые полгода - 65000грн в месяц)
Татьяна Сергеевна, менеджер по продажам (получает 1000грн + 3% от продаж, касса аналогична Кореньковой Анне).

 */
public class Main {

    public static void main(String[] args) {

        Employee[] employees = Generator.generateEmployees();

        for (int i = 0; i < 12; i++) {
            System.out.println("Месяц "+(i+1)+":");
            for (int j = 0; j < employees.length; j++) {

                System.out.println(employees[j].name+" : "+employees[j].post);
                System.out.println("За месяц "+(i+1)+": "+employees[j].getMonthValue());
                System.out.println("За год: "+employees[j].getYearValue());
            }

        }


    }
}
